package com.gk;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gk.config.XMLConverter;
import com.gk.model.Root;
import com.gk.model.XPS;

@SpringBootApplication
public class UnmarshallingToMarshallingMasterApplication {

	public static void main(String[] args) throws IOException {
//		SpringApplication.run(UnmarshallingToMarshallingMasterApplication.class, args);
		ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		XMLConverter xmlConverter = (XMLConverter) context.getBean(XMLConverter.class);
		String filePath = "G:\\Ganesh\\workspace-2020\\unmarshalling-to-marshalling-master\\src\\main\\java\\policydata.xml";
		System.out.println("START XML FILE CONVERSION ");
		XPS root = (XPS) xmlConverter.convertFromXMLToObject(filePath);
		System.out.println(root.toString());
		System.out.println("END XML FILE CONVERSION ");
		
		ObjectMapper objectMapper = new ObjectMapper();
		String xps = objectMapper.writeValueAsString(root);
		System.out.println(xps);
	}

}
