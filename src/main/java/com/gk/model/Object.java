package com.gk.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Object implements Serializable{

	private static final long serialVersionUID = 3968502307622935518L;

	@XmlAttribute(name="UpdatedBy")
	private String UpdatedBy;

	@XmlAttribute(name="Xid")
	private String Xid;

	@XmlAttribute(name="ExportType")
	private String ExportType;
	@XmlAttribute(name="Class")
	private String Class;

	@XmlAttribute(name="UpdateMethod")
	private String UpdateMethod;
	
//	private List<Properties> Property = new ArrayList<>();

	@XmlElement
	private Property[] Property;

	@XmlAttribute(name="CreatedDateTime")
	private String CreatedDateTime;

	@XmlAttribute(name="ModifiedDateTime")
	private String ModifiedDateTime;

	public String getUpdatedBy() {
		return UpdatedBy;
	}

	public void setUpdatedBy(String UpdatedBy) {
		this.UpdatedBy = UpdatedBy;
	}

	public String getXid() {
		return Xid;
	}

	public void setXid(String Xid) {
		this.Xid = Xid;
	}

	public String getExportType() {
		return ExportType;
	}

	public void setExportType(String ExportType) {
		this.ExportType = ExportType;
	}

	public String getClasS() {
		return Class;
	}

	
	public void setClass(String Class) {
		this.Class = Class;
	}

	public String getUpdateMethod() {
		return UpdateMethod;
	}

	public void setUpdateMethod(String UpdateMethod) {
		this.UpdateMethod = UpdateMethod;
	}

	public Property[] getProperty() {
		return Property;
	}

	public void setProperty(Property[] Property) {
		this.Property = Property;
	}

	public String getCreatedDateTime() {
		return CreatedDateTime;
	}

	public void setCreatedDateTime(String CreatedDateTime) {
		this.CreatedDateTime = CreatedDateTime;
	}

	public String getModifiedDateTime() {
		return ModifiedDateTime;
	}

	public void setModifiedDateTime(String modifiedDateTime) {
		ModifiedDateTime = modifiedDateTime;
	}

	
//	public List<Properties> getProperty() {
//		return Property;
//	}
//
//	@XmlJavaTypeAdapter(PropertyAdapter.class)
//	public void setProperty(List<Properties> property) {
//		this.Property = property;
//	}


	@Override
	public String toString() {
		return "ClassPojo [UpdatedBy = " + UpdatedBy + ", Xid = " + Xid + ", ExportType = " + ExportType + ", Class = "
				+ Class + ", UpdateMethod = " + UpdateMethod + ", Property = " + Property + ", CreatedDateTime = "
				+ CreatedDateTime + "]";
	}
}
