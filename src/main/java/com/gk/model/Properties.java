package com.gk.model;

import javax.xml.bind.annotation.XmlAttribute;

public abstract class Properties {

	private String name;

	@XmlAttribute(name="Name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
