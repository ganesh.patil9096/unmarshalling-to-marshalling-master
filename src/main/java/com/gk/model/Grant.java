package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Grant {
	@XmlElement
	private String Rights;

	@XmlElement
    private String SecurityCategory;

    public String getRights ()
    {
        return Rights;
    }

    public void setRights (String Rights)
    {
        this.Rights = Rights;
    }

    public String getSecurityCategory ()
    {
        return SecurityCategory;
    }

    public void setSecurityCategory (String SecurityCategory)
    {
        this.SecurityCategory = SecurityCategory;
    }

    @Override
    public String toString()
    {
        return "[Rights = "+Rights+", SecurityCategory = "+SecurityCategory+"]";
    }
}
