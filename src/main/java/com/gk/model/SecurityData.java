package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SecurityData {
	@XmlElement
	private Administrator Administrator;

	public Administrator getAdministrator ()
    {
        return Administrator;
    }

    public void setAdministrator (Administrator Administrator)
    {
        this.Administrator = Administrator;
    }

    @Override
    public String toString()
    {
        return "[Administrator = "+Administrator+"]";
    }
}
