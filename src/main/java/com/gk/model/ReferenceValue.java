package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ReferenceValue")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReferenceValue {
	@XmlAttribute(name="ReferenceId")
	private String ReferenceId;

	@XmlElement
    private String StringValue;

    public String getReferenceId ()
    {
        return ReferenceId;
    }

    public void setReferenceId (String ReferenceId)
    {
        this.ReferenceId = ReferenceId;
    }

    public String getStringValue ()
    {
        return StringValue;
    }

    public void setStringValue (String StringValue)
    {
        this.StringValue = StringValue;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ReferenceId = "+ReferenceId+", StringValue = "+StringValue+"]";
    }
}
