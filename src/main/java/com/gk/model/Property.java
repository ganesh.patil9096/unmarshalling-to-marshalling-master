package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Property {
	@XmlElement
	private LinkValue LinkValue;

	@XmlAttribute(name="Name")
    private String Name;

    public LinkValue getLinkValue ()
    {
        return LinkValue;
    }

    public void setLinkValue (LinkValue LinkValue)
    {
        this.LinkValue = LinkValue;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LinkValue = "+LinkValue+", Name = "+Name+"]";
    }
}
