package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Vendor {
	@XmlElement
	private Product[] Product;

	@XmlAttribute(name="Name")
    private String Name;

    public Product[] getProduct ()
    {
        return Product;
    }

    public void setProduct (Product[] Product)
    {
        this.Product = Product;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Product = "+Product+", Name = "+Name+"]";
    }
}
