package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Property")
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjectProperty {

	@XmlElement
	private String NumberValue;

	@XmlAttribute
    private String Name;

    public String getNumberValue ()
    {
        return NumberValue;
    }

    public void setNumberValue (String NumberValue)
    {
        this.NumberValue = NumberValue;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [NumberValue = "+NumberValue+", Name = "+Name+"]";
    }
}
