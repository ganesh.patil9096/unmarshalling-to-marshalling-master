package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LinkValue {
	@XmlElement
	private String XREF;

    public String getXREF ()
    {
        return XREF;
    }

    public void setXREF (String XREF)
    {
        this.XREF = XREF;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [XREF = "+XREF+"]";
    }
}
