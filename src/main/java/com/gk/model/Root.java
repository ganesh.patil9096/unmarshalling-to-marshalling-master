package com.gk.model;

import javax.xml.bind.annotation.XmlRootElement;


public class Root {
	
		
	private XPS XPS;

    public XPS getXPS ()
    {
        return XPS;
    }

    public void setXPS (XPS XPS)
    {
        this.XPS = XPS;
    }

    @Override
    public String toString()
    {
        return "Root [XPS = "+XPS+"]";
    }
}
