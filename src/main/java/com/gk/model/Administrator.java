package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Administrator {
	@XmlElement
	private Grant Grant;

	@XmlElement
    private String Description;

    @XmlElement
    private String UserPath;

    @XmlElement
    private String MethodsAllowed;

    @XmlElement
    private String Workspaces;

    @XmlElement
    private String Name;

    public Grant getGrant ()
    {
        return Grant;
    }

    public void setGrant (Grant Grant)
    {
        this.Grant = Grant;
    }

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getUserPath ()
    {
        return UserPath;
    }

    public void setUserPath (String UserPath)
    {
        this.UserPath = UserPath;
    }

    public String getMethodsAllowed ()
    {
        return MethodsAllowed;
    }

    public void setMethodsAllowed (String MethodsAllowed)
    {
        this.MethodsAllowed = MethodsAllowed;
    }

    public String getWorkspaces ()
    {
        return Workspaces;
    }

    public void setWorkspaces (String Workspaces)
    {
        this.Workspaces = Workspaces;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    @Override
    public String toString()
    {
        return "[Grant = "+Grant+", Description = "+Description+", UserPath = "+UserPath+", MethodsAllowed = "+MethodsAllowed+", Workspaces = "+Workspaces+", Name = "+Name+"]";
    }
}
