package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="References", namespace="com.gk.model")
@XmlAccessorType(XmlAccessType.FIELD)
public class References {
	@XmlElement
	private ReferenceValue[] ReferenceValue;

    public ReferenceValue[] getReferenceValue ()
    {
        return ReferenceValue;
    }

    public void setReferenceValue (ReferenceValue[] ReferenceValue)
    {
        this.ReferenceValue = ReferenceValue;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ReferenceValue = "+ReferenceValue+"]";
    }
}
