package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Product {
	
	@XmlElement
	private ParameterName[] ParameterName;

	@XmlAttribute(name="Name")
    private String Name;

    public ParameterName[] getParameterName ()
    {
        return ParameterName;
    }

    public void setParameterName (ParameterName[] ParameterName)
    {
        this.ParameterName = ParameterName;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ParameterName = "+ParameterName+", Name = "+Name+"]";
    }
}
