package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfigurationData {
	@XmlElement
	private Vendor Vendor;

    public Vendor getVendor ()
    {
        return Vendor;
    }

    public void setVendor (Vendor Vendor)
    {
        this.Vendor = Vendor;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Vendor = "+Vendor+"]";
    }
}
