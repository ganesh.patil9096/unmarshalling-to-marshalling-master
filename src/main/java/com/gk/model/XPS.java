package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="XPS")
@XmlAccessorType(XmlAccessType.FIELD)
public class XPS {
	@XmlElement
	private PolicyData PolicyData;

	@XmlElement
    private SecurityData SecurityData;

    @XmlElement
    private String XPSAdmin;

    @XmlElement
    private String XPSVersion;
    
    @XmlElement
    private String HostInformation;

    public PolicyData getPolicyData ()
    {
        return PolicyData;
    }

    public void setPolicyData (PolicyData PolicyData)
    {
        this.PolicyData = PolicyData;
    }

    public SecurityData getSecurityData ()
    {
        return SecurityData;
    }

    public void setSecurityData (SecurityData SecurityData)
    {
        this.SecurityData = SecurityData;
    }

    public String getXPSAdmin ()
    {
        return XPSAdmin;
    }

    public void setXPSAdmin (String XPSAdmin)
    {
        this.XPSAdmin = XPSAdmin;
    }

    public String getXPSVersion ()
    {
        return XPSVersion;
    }

    public void setXPSVersion (String XPSVersion)
    {
        this.XPSVersion = XPSVersion;
    }

    public String getHostInformation ()
    {
        return HostInformation;
    }

    public void setHostInformation (String HostInformation)
    {
        this.HostInformation = HostInformation;
    }
    

    @Override
	public String toString() {
		return "XPS [SecurityData=" + SecurityData + ", XPSAdmin=" + XPSAdmin + ", XPSVersion=" + XPSVersion
				+ ", HostInformation=" + HostInformation + "]";
	}
}
