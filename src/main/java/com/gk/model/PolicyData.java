package com.gk.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="PolicyData")
@XmlAccessorType(XmlAccessType.FIELD)
public class PolicyData {
	@XmlElement
	private References References;

	@XmlAttribute(name="IsDumpExport")
    private String IsDumpExport;

	@XmlElement
    private ConfigurationData ConfigurationData;

	@XmlElement
    private Object[] Object;

    public References getReferences ()
    {
        return References;
    }

    public void setReferences (References References)
    {
        this.References = References;
    }

    public String getIsDumpExport ()
    {
        return IsDumpExport;
    }

    public void setIsDumpExport (String IsDumpExport)
    {
        this.IsDumpExport = IsDumpExport;
    }

    public ConfigurationData getConfigurationData ()
    {
        return ConfigurationData;
    }

    public void setConfigurationData (ConfigurationData ConfigurationData)
    {
        this.ConfigurationData = ConfigurationData;
    }

    public Object[] getObject ()
    {
        return Object;
    }

    public void setObject (Object[] Object)
    {
        this.Object = Object;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [References = "+References+", IsDumpExport = "+IsDumpExport+", ConfigurationData = "+ConfigurationData+", Object = "+Object+"]";
    }
}
